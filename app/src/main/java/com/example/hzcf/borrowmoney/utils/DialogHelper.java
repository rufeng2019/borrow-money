package com.example.hzcf.borrowmoney.utils;

import android.app.Dialog;
import android.text.TextUtils;

import com.example.hzcf.borrowmoney.customview.SDProgressDialog;
import com.example.hzcf.borrowmoney.model.BaseActModel;

public class DialogHelper
{
	public static void setDialogMsg(Dialog sdProgressDialog, BaseActModel model)
	{
		if (sdProgressDialog != null && model != null && sdProgressDialog instanceof SDProgressDialog && !TextUtils.isEmpty(model.getShow_err()))
		{
			SDProgressDialog sdDialog = (SDProgressDialog) sdProgressDialog;
			sdDialog.getmTxtMsg().setText(model.getShow_err());
		}
	}

}
