package com.example.hzcf.borrowmoney.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;

import com.example.hzcf.borrowmoney.fragment.BaseFragment;
import com.example.hzcf.borrowmoney.utils.DialogUtil;
import com.umeng.analytics.MobclickAgent;
import com.zhy.autolayout.AutoLayoutActivity;


public abstract class BaseActivity extends AutoLayoutActivity
{
	
	public DialogUtil mDialogUtil = null;
	public Dialog mBaseDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		baseInit();
	}

	public void setSdContentView(int resViewId)
	{
		super.setContentView(resViewId);
	}

	public void addFragment(BaseFragment fragment, int containerId)
	{
		getSupportFragmentManager().beginTransaction().add(containerId, fragment).commitAllowingStateLoss();
	}

	public void replaceFragment(BaseFragment fragment, int containerId)
	{
		getSupportFragmentManager().beginTransaction().replace(containerId, fragment).commitAllowingStateLoss();
	}


    private void baseInit() {
        //去掉actionBar
//        getSupportActionBar().hide();
        int layoutId = getLayoutId();
        if (layoutId != 0) {
            this.setContentView(layoutId);
        }

		mDialogUtil = new DialogUtil(this);
		ActivityManager.getInstance().addActivity(this);
	}

	/**
	 * 布局文件ID
	 * @return
     */
	protected abstract int getLayoutId();

	public void showLoadingDialog(String msg)
	{
		if (msg != null)
		{
			if (mBaseDialog != null && mBaseDialog.isShowing())
			{
				mBaseDialog.dismiss();
				mBaseDialog = null;
			}
			mBaseDialog = mDialogUtil.showLoading(msg);
		}
	}

	public void hideLoadingDialog()
	{
		if (mBaseDialog != null && mBaseDialog.isShowing())
		{
			mBaseDialog.dismiss();
		}
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		ActivityManager.getInstance().finishActivity(this);
	}



	@Override
	protected void onPause()
	{
		super.onPause();
		// session的统计
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		// session的统计
		MobclickAgent.onResume(this);

	}


	//返回键返回事件
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (KeyEvent.KEYCODE_BACK == keyCode) {
			if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
				finish();
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}


}
