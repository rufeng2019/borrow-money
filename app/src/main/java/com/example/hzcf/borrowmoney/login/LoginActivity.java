package com.example.hzcf.borrowmoney.login;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.hzcf.borrowmoney.R;
import com.example.hzcf.borrowmoney.activity.BaseActivity;
import com.example.hzcf.borrowmoney.utils.LogUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by zrb on 2016/10/9.
 */

public class LoginActivity extends BaseActivity implements View.OnLayoutChangeListener {
    private FrameLayout rootView;
    private int keyHeight;
    private int ivHeight;
    private ImageView iv;
    @Override
    protected int getLayoutId() {
        return R.layout.act_login;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = (FrameLayout) findViewById(R.id.root);
        iv = (ImageView) findViewById(R.id.imageView);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        keyHeight = dm.heightPixels / 3;
        rootView.addOnLayoutChangeListener(this);
        //添加监听获取宽高
        final ViewTreeObserver vto = iv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ivHeight = iv.getMeasuredHeight();
//                避免重复添加监听
               iv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    LinearLayout.LayoutParams lp;

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        LogUtil.e("left=" + left + "----top=" + top + "----right=" + right + "----bottom=" + bottom);
        LogUtil.e("oldLeft=" + oldLeft + "----oldTop=" + oldTop + "----oldRight=" + oldRight + "----oldBottom=" + oldBottom);
        if (oldBottom != 0 && bottom != oldBottom) {//排除activity刚进来时的变化，以及没有变化时的情况
            //当oldBottom和bottom的变化范围超过1/3屏幕时，我们视为键盘发生的弹出或收起。
            if (oldBottom - bottom > keyHeight) {//弹出
                ;
                hideAnim();

            } else if (bottom - oldBottom > keyHeight) {//收起

                showAnim();
            }
        }
    }

    private void hideAnim() {
        ValueAnimator anim = ValueAnimator.ofInt(ivHeight, 0);
        anim.setDuration(500);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (int) animation.getAnimatedValue();
                if(ivHeight%2==0){
                    lp = (LinearLayout.LayoutParams) iv.getLayoutParams();
                    lp.height = value;
                    iv.setLayoutParams(lp);
                }
            }
        });
        anim.start();
    }

    private void showAnim() {
        ValueAnimator anim = ValueAnimator.ofInt(0, ivHeight);
        anim.setDuration(500);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (int) animation.getAnimatedValue();
                if(value==ivHeight||ivHeight%2==0){
                    lp = (LinearLayout.LayoutParams) iv.getLayoutParams();
                    lp.height = value;
                    iv.setLayoutParams(lp);
                }

            }
        });
        anim.start();
    }
}
