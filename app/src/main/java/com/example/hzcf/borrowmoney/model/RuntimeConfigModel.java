package com.example.hzcf.borrowmoney.model;


public class RuntimeConfigModel
{

	private boolean isMainActivityStarted = false;

	public boolean isMainActivityStarted()
	{
		return isMainActivityStarted;
	}

	public void setMainActivityStarted(boolean isMainActivityStarted)
	{
		this.isMainActivityStarted = isMainActivityStarted;
	}

}
