package com.example.hzcf.borrowmoney.listener;

public interface IVisibleStateListener
{

	public void onShowView();

	public void onHideView();

}
