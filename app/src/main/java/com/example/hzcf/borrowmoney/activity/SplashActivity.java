package com.example.hzcf.borrowmoney.activity;

import android.os.Bundle;
import android.os.Handler;

import com.example.hzcf.borrowmoney.R;
import com.example.hzcf.borrowmoney.utils.LogUtil;

import java.util.HashSet;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

/**
 * Created by zrb on 2016/10/12.
 */

public class SplashActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.act_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
