package com.example.hzcf.borrowmoney.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hzcf.borrowmoney.activity.BaseActivity;
import com.example.hzcf.borrowmoney.activity.MainActivity;


public abstract class BaseFragment extends Fragment  {

    protected abstract void initView(View view, Bundle savedInstanceState);

    //获取fragment布局文件ID
    protected abstract int getLayoutId();

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public MainActivity getMainActivity() {
        BaseActivity activity = getBaseActivity();
        if (activity != null && (activity instanceof MainActivity)) {
            return ((MainActivity) activity);
        } else {
            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        initView(view, savedInstanceState);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
